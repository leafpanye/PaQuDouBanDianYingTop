import requests
import urllib.parse
import urllib.request
from bs4 import BeautifulSoup

def get_movies():
    headers = {
        'user-agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko)'
                      ' Chrome/58.0.3029.110 Safari/537.36',
        'Host': 'movie.douban.com'
    }
    movie_list = []
    for i in range(0, 10):
        link = 'https://movie.douban.com/top250?start=' + str(i * 25)
        r = requests.get(link, headers=headers, timeout=10)
        print(str(i + 1), "页响应状态码:", r.status_code)

        soup = BeautifulSoup(r.text, 'html.parser')
        div_list = soup.find_all('div', class_='pic')
        for each in div_list:
            movie = each.a.img.get('src')
            movie_list.append(movie)
    i = 0
    for image in movie_list:
        print(image)
        i = i + 1
        urllib.request.urlretrieve(image, 'C:\\Users\\Administrator\\Desktop\\PY-TEST\\photo\\' + str(i) + '.jpg')

get_movies()